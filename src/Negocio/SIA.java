/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Estudiante;
import Modelo.Semestre;
import ufps.util.colecciones_seed.ListaCD;
import ufps.util.varios.ArchivoLeerURL;

/**
 *
 * @author madar
 */
public class SIA {

    private Semestre semestres[] = new Semestre[10];

    public SIA() {
        this.crearSemestres();
    }

    public void cargarDatos(String url) {
        ArchivoLeerURL archivo = new ArchivoLeerURL(url);
        Object datos[] = archivo.leerArchivo();
        //Esté índice en 1, por que la primera línea
        //Contiene el formato del archivo
        for (int i = 1; i < datos.length; i++) {
            //codigos;nombre de estudiante;email;semestre
            String datoFila = datos[i].toString();
            String datoEstudiante[] = datoFila.split(";");
            int cod = Integer.parseInt(datoEstudiante[0]);
            String nombre = datoEstudiante[1];
            String email = datoEstudiante[2];
            short semestre = Short.parseShort(datoEstudiante[3]);
            semestre--;
            this.semestres[semestre].agregarEstudiante(cod, nombre, email);
            //System.out.println(fila.toString());
        }

    }

    private void crearSemestres() {
        /*
            for(Semestre s: semestres)
                s=new Semestre()??? 
                --> :(
                --> Error
         */
        for (int i = 0; i < semestres.length; i++) {
            this.semestres[i] = new Semestre();

        }
    }

    @Override
    public String toString() {
        String msg = "";

        int s = 1;
        for (Semestre datoSemestre : this.semestres) {

            if (datoSemestre.contieneEstudiantes()) {
                msg += "\n Semestre " + s + ":\n" + datoSemestre.toString();
            } else {
                msg += "El semestre:" + s + ", no contiene estudiantes";
            }
            s++;
        }
        return msg;
    }

    
    public String getListado_Por_Semestre(short s)
    {
        if(s<=0 || s>10)
            throw new RuntimeException("Semestre inválido");
        Semestre datoSemestre=this.semestres[s-1];
        if(datoSemestre.contieneEstudiantes())
            return datoSemestre.toString();
        return "El semestre "+s+", NO contiene estudiantes";
    }
    
    
    /**
     * Obtiene el(los) semestres con la mayor cantidad
     * de estudiantes
     * @return un String del tipo: Semestre-x, semestre-y,...
     */
    public String getSemestre_Mas_Estudiantes()
    {
        return "";
    }
}
