/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ufps.util.colecciones_seed;

/**
 *
 * @author madar
 */
public class Pila<T> {

    //Decorador:
    private ListaCD<T> tope = new ListaCD();

    public Pila() {
    }

    /**
     * Apila un elemento en el tope de la pila
     *
     * @param info el objeto a insertar en la pilaF
     */
    public void push(T info) {
        this.tope.insertarInicio(info);
    }

    public T pop() {
        return this.tope.eliminar(0);
    }

    public int getTamanio() {
        return this.tope.getTamanio();
    }

    public boolean esVacia() {
        return this.getTamanio()==0;
    }
}
