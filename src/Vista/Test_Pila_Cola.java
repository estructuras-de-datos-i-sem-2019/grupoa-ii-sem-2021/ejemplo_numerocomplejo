/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import ufps.util.colecciones_seed.Cola;
import ufps.util.colecciones_seed.Pila;

/**
 *
 * @author madarme
 */
public class Test_Pila_Cola {

    public static void main(String nada[]) {
        String nombre = "Anita lava la tina";
        Pila<Character> p = crearPila(nombre);
        Cola<Character> c = crearCola(nombre);
        String m, n;
        m = n = "";
        while (!p.esVacia()) {
            m += p.pop() + "\t";
            n += c.deColar() + "\t";
        }
        System.out.println(m + "\n" + n);
        if (esPalindrome(nombre.toLowerCase())) {
            System.out.println("Si es palíndroma");
        } else {
            System.out.println("No es palíndroma");
        }

        String cadena = "( 8+9, 5, 6)((3,4,5+69*i-9)))";
        if (tieneParentesisBalanceados(cadena)) {
            System.out.println("Tiene parèntesis balenceados");
        } else {
            System.out.println("No Tiene parèntesis balenceados");
        }

    }

    private static boolean esPalindrome(String nombre) {
        Pila<Character> p = crearPila(nombre);
        Cola<Character> c = crearCola(nombre);

        while (!p.esVacia()) {
            if (p.pop() != c.deColar()) {
                return false;
            }
        }
        return true;
    }

    private static Pila<Character> crearPila(String cadena) {
        Pila<Character> p = new Pila();

        for (int i = 0; i < cadena.length(); i++) {
            char x = cadena.charAt(i);
            if (!Character.isWhitespace(x)) {
                p.push(x);

            }
        }
        return p;
    }

    private static Cola<Character> crearCola(String cadena) {

        Cola<Character> c = new Cola();
        for (int i = 0; i < cadena.length(); i++) {
            char x = cadena.charAt(i);
            if (!Character.isWhitespace(x)) {
                c.enColar(x);
            }
        }
        return c;
    }

    private static boolean tieneParentesisBalanceados(String expresion) {

        Pila<Character> p = new Pila();
        for (int i = 0; i < expresion.length(); i++) {
            char dato = expresion.charAt(i);
            if (dato == '(') {
                p.push(dato);
            } else {

                if (dato == ')') {
                    if (!p.esVacia()) {
                        dato = p.pop();
                    } else {
                        return false;
                    }
                }

            }
        }

        return p.esVacia();
    }
}
