/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.NumeroComplejo;

/**
 *
 * @author madar
 */
public class Test_Genericidad {

    public static void main(String[] args) {
        String v[] = {"madarme", "anderson", "yeiner"};
        Integer v2[] = {4, 6, 7, 8, 9, 0};
        NumeroComplejo v3[] = {new NumeroComplejo(3, 4), new NumeroComplejo(13, 4)};
        imprimirVector(v);
        imprimirVector(v2);
        imprimirVector(v3);
    }

    /**
     * Método génerico que imprime cualquier vector sin importar su tipo
     * @param <T> el tipode dato
     * @param vector la colección
     */
    private static <T> void imprimirVector(T vector[]) {

        String msg = "";
        for (T dato : vector) {
            msg += dato.toString() + "\t";
        }
        System.out.println(msg);
    }

}
